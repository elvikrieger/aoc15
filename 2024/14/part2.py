

robots = []

with open("puzzle_input.txt") as f:
    for line in f:
        robot_line = line.strip().split()
        position, velocity = robot_line
        position = tuple(map(int, position.replace("p=", "").split(",")))
        velocity = tuple(map(int, velocity.replace("v=", "").split(",")))
        # robot = (position, velocity)
        robot = {"position": position,
                 "velocity": velocity}
        robots.append(robot)


cols = 101
rows = 103

def change_position(robots):
    for robot in robots:
        c,r = robot["position"]
        pc, pr = robot["velocity"]
        posun_c = (pc + c) % cols
        posun_r = (pr + r)% rows
        robot["position"] = (posun_c, posun_r)
    return robots

def get_coords(robots_list):
    coords = []
    for robot in robots:
        coords.append(robot["position"])
    return coords

for i in range(100000000):
    actual_coords = get_coords(robots)

    actual_coords.sort()
    p_r = -1
    neighbors = 0
    for c, r in actual_coords:
        if r - p_r == 1:
            neighbors += 1
        p_r = r

    if neighbors > 100:
        ohradka = []
        robots_coords = []
        for _ in range(rows):
            ohradka.append([])
            for __ in range(cols):
                ohradka[-1].append(".")

        for coord in actual_coords:
            c, r = coord
            ohradka[r][c] = "#"

        for row in ohradka:
            print(" ".join(row))
        print(i)
        input()
    robots = change_position(robots)

# def position_after_seconds(bot, seconds):
#     c, r = bot["position"]
#     pc, pr = bot["velocity"]
#     new_coords = ((pc * seconds + c) % number_of_colums, (pr * seconds + r) % number_of_rows)
#     bot["position"] = new_coords
#     return bot
