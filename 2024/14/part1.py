robots = []

with open("puzzle_input.txt") as f:
    for line in f:
        robot_line = line.strip().split()
        position, velocity = robot_line
        position = tuple(map(int, position.replace("p=", "").split(",")))
        velocity = tuple(map(int, velocity.replace("v=", "").split(",")))
        # robot = (position, velocity)
        robot = {"position": position,
                 "velocity": velocity}
        robots.append(robot)

number_of_rows = 103
number_of_colums = 101

polovina_radku = number_of_rows // 2
polovina_sloupcu = number_of_colums // 2

def position_after_seconds(bot, seconds):
    c, r = bot["position"]
    pc, pr = bot["velocity"]
    new_coords = ((pc * seconds + c) % number_of_colums, (pr * seconds + r) % number_of_rows)
    bot["position"] = new_coords
    return bot

seconds = 100

q1 = 0
q2 = 0
q3 = 0
q4 = 0
q5 = 0
ohradka = []
for _ in range(number_of_rows):
    ohradka.append([])
    for __ in range(number_of_colums):
        ohradka[-1].append(".")

for robot in robots:
    robot = position_after_seconds(robot, seconds)
    c, r = robot["position"]
    if c == polovina_sloupcu or r == polovina_radku:
        q5 += 1
    elif c < polovina_sloupcu and r < polovina_radku:
        q1 += 1
    elif c > polovina_sloupcu and r < polovina_radku:
        q2 += 1
    elif c < polovina_sloupcu and r > polovina_radku:
        q3 += 1
    elif c > polovina_sloupcu and r > polovina_radku:
        q4 += 1
    else:
        print(robot)

    if ohradka[r][c] == ".":
        ohradka[r][c] = str(1)
    else:
        ohradka[r][c] = str(int(ohradka[r][c]) + 1)

result = q1*q2*q3*q4
print(result)