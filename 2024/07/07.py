from pprint import pprint

puzzle = {}
with open("puzzle_input.txt") as f:
# with open("test.txt") as f:
    for line in f:
        r_test_value, r_numbers =line.split(":")
        if int(r_test_value) not in puzzle:
            puzzle[int(r_test_value)] = []
        puzzle[int(r_test_value)].append([int(number) for number in r_numbers.split()])


def is_possible(n, parts):
    if len(parts) == 1:
        return n == parts[0]
    else:
        remaining = parts[2:]

        option_plus = parts[0]+parts[1]
        option_mult = parts[0]*parts[1]
        # part 2
        option_conc = int(str(parts[0])+str(parts[1]))

        return any([is_possible(n, [option_plus]+remaining), is_possible(n, [option_mult]+remaining), is_possible(n, [option_conc]+remaining)])

result = 0
for number in puzzle:
    for list in puzzle[number]:
        if is_possible(number, list):
            result +=number
print(result)