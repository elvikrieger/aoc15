# with open("test.txt") as f:
with open("puzzle_input.txt") as f:
    source = f.read()
    source = source.strip().split("\n\n")

rovnice = []

for part in source:
    line = part.split("\n")
    raw_a = line[0].replace("Button A: ", "").strip().split()
    button_a_x = int(raw_a[0].replace("X+", "").replace(",", ""))
    button_a_y = int(raw_a[1].replace("Y+", "").replace(",", ""))
    button_a = (button_a_x, button_a_y)

    raw_b = line[1].replace("Button B: ", "").strip().split()
    button_b_x = int(raw_b[0].replace("X+", "").replace(",", ""))
    button_b_y = int(raw_b[1].replace("Y+", "").replace(",", ""))
    button_b = (button_b_x, button_b_y)

    raw_p = line[2].replace("Prize: ", "").strip().split()
    prize_x = int(raw_p[0].replace("X=", "").replace(",", ""))
    prize_y = int(raw_p[1].replace("Y=", "").replace(",", ""))
    prize = (prize_x, prize_y)

    rovnice.append((button_a, button_b, prize))


def find_solution(a, b, r):
    xa, ya = a
    xb, yb = b
    xr, yr = r

    b = (yr * xa - ya * xr) / (xa * yb - ya * xb)
    a = (xr - xb * b) / xa
    if int(b) != b or int(a) != a:
        return 0
    if a > 100 or a < 0 or b > 100 or b < 0:
        return 0
    return int(a * 3 + b)

tokens = 0
for a, b, p in rovnice:
    tokens += find_solution(a, b, p)

print(tokens)
"""

xr = xa*A + xb*B  ->  A = (xr-xb*B) /xa 
yr = ya*A + yb*B  ->  yr = ya*(xr-xb*B) /xa 
yr/ 

"""

def find_solution_p2(a, b, r):
    xa, ya = a
    xb, yb = b
    xr, yr = r
    xr += 10000000000000
    yr += 10000000000000

    b = (yr * xa - ya * xr) / (xa * yb - ya * xb)
    a = (xr - xb * b) / xa
    if int(b) != b or int(a) != a:
        return 0
    if a < 0 or b < 0:
        return 0
    return int(a * 3 + b)

tokens = 0
for a, b, p in rovnice:
    tokens += find_solution_p2(a, b, p)

print(tokens)

# print(source)
