import re

def first(text):
    pattern = r"mul\((\d{1,3}),(\d{1,3})\)"
    matches = re.findall(pattern, text)
    return sum(int(nums[0]) * int(nums[1]) for nums in matches)

test_string = "xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))"
print(first(test_string))


with open("puzzle_input.txt") as f:
    text = f.read()
print(first(text))


# part two
test_string = "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))"
def second(text):
    new_pattern = r"(mul\(\d{1,3},\d{1,3}\)|don't\(\)|do\(\))"
    matches = re.findall(new_pattern, text)
    print(matches)
    enabled = True
    enabled_matches = []
    for match in matches:
        if match not in ["don't()", "do()"] and enabled:
            enabled_matches.append(match)
        if match == "don't()":
            enabled = False
        elif match == "do()":
            enabled = True
    return first("".join(enabled_matches))


print(second(text))