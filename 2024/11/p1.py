from pprint import pprint

stones = "1 24596 0 740994 60 803 8918 9405859"

test_stones = "0 1 10 99 999"
# test_stones = "125 17"

# stones = test_stones

stones = stones.split()
# print(stones)
stone_dict = {"0": ["1"]}


def blink(stone_list):
    new_stones = []
    for stone in stone_list:
        if stone in stone_dict:
            new_stones.extend(stone_dict[stone])
        else:
            if stone == "0":
                new_stones.append("1")
            elif len(stone) % 2 == 0:
                middle = int((len(stone)) / 2)
                added_stones = [stone[:middle], stone[middle:]]
                left = stone[:middle].lstrip("0") or "0"
                right = stone[middle:].lstrip("0") or "0"
                new_stones.extend([left, right])
                stone_dict[stone] = [left, right]
            else:
                result = str(int(stone) * 2024)
                new_stones.append(str(int(stone) * 2024))
                stone_dict[stone] = [result]
    return new_stones


blinks = 25
for i in range(blinks):
    stones = blink(stones)
p1 = len(stones)


# part 2

def blink(current_stone):
    if current_stone in stone_dict:
        return stone_dict[current_stone]
    else:
        current_stone = str(current_stone)
        if len(str(current_stone)) % 2 == 0:
            middle = len(str(current_stone)) // 2
            left = current_stone[:middle].lstrip("0") or "0"
            right = current_stone[middle:].lstrip("0") or "0"
            stone_dict[current_stone] = [left, right]
            return left, right
        else:
            result = int(current_stone) * 2024
            stone_dict[current_stone] = [result]
            return [str(result)]


cache = {}


def count_splitted_stones(stones, blinks):
    if blinks == 0:
        return 1
    key = (tuple(stones), blinks)
    if key in cache:
        return cache[key]

    result = 0
    for stone in stones:
        result += count_splitted_stones(blink(stone), blinks - 1)
    cache[key] = result
    return result


stones = "1 24596 0 740994 60 803 8918 9405859"

stones = stones.split()
blinks = 75
p2 = 0

for stone in stones:
    p2 += count_splitted_stones([stone], blinks)
print(p2)
print(len(cache))



