from collections import defaultdict
from pprint import pprint

city = []
with open("puzzle_input.txt") as f:
# with open("test.txt")as f:
    for line in f:
        city.append(list(line.strip()))

antennas = defaultdict(list)
for ir in range(len(city)):
    for ic in range(len(city[0])):
        if city[ir][ic] != ".":
            antennas[city[ir][ic]].append((ir, ic))


# pprint(antennas)


def find_antinodes(antenna_a, antenna_b):
    """ vrati souradnice antinodes """
    ar, ac = antenna_a
    br, bc = antenna_b

    delta_r = br - ar
    delta_c = bc - ac

    antinode1 = (ar - delta_r, ac - delta_c)
    antinode2 = (br + delta_r, bc + delta_c)


    return antinode1, antinode2

def is_in_city(city, node):
    return 0<= node[0] < len(city) and 0<=node[1]<len(city[0])


antinodes =set()
for freq in antennas:
    visited = []
    for a in range(len(antennas[freq])):
        for b in range(len(antennas[freq])):
            if a != b and (a, b) not in visited and (b, a) not in visited: # nesmim hledat node pro ten stejny bod
                visited.extend([(a,b),(b,a)])
                node1, node2= find_antinodes(antennas[freq][a], antennas[freq][b])
                for node in [node1, node2]:
                    if is_in_city(city, node):
                        antinodes.add(node)

print(len(antinodes))
