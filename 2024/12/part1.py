from collections import deque
from pprint import pprint

with open("puzzle_input") as f:
# with open("test2.txt") as f:
    raw_garden = [list(line.strip()) for line in f]


garden = raw_garden

for row in garden:
    print(row)

checked = set()

def count_area_and_perimeter(coords):
    current_plant = garden[coords[0]][coords[1]]
    current_field = set()
    area = 0
    perimeter = 0
    to_check = deque([coords])

    directions = [(-1, 0), (0, 1), (1, 0), (0, -1)]
    while to_check:
        r, c = to_check.popleft()
        if (r, c) in checked:
            continue
        checked.add((r, c))
        area += 1
        for dr, dc in directions:
            nr, nc = r + dr, c + dc
            if 0 <= nr < len(garden) and 0 <= nc < len(garden[0]):
                if garden[nr][nc] == current_plant:
                    if (nr, nc) not in checked:
                        to_check.append((nr, nc))
                    current_field.add((nr,nc))
                else:
                    perimeter += 1
            else:
                perimeter += 1
    print(current_plant, current_field)

    return area, perimeter

total_price = 0
for r in range(len(garden)):
    for c in range(len(garden[0])):
        if (r, c) not in checked and garden[r][c] != "-":
            area, perimeter = count_area_and_perimeter((r, c))
            total_price += area * perimeter

print(total_price)

assert total_price == 140 # test2
# assert total_price == 1550156
# ----------------------------------------------------------------------------------------------------------------------
# part 2






