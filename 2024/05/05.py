from pprint import pprint
import random

with open("puzzle_input.txt") as f:
    # with open("test.txt") as f:
    puzzle = f.read()

rules, updates = puzzle.split("\n\n")

updates = updates.splitlines()
updates = [[int(x) for x in line.split(",")] for line in updates]

# Zpracování rules
result = []
for line in rules.splitlines():
    x, y = line.split("|")
    result.append((int(x), int(y)))
rules = result


def is_update_order_correct(rules, update):
    for x, y in rules:
        if x in update and y in update:
            if update.index(x) > update.index(y):
                return False
    return True


def get_middle(update):
    return update[len(update) // 2]


sum = 0
incorrect_updates = []
for update in updates:
    if is_update_order_correct(rules, update):
        sum += get_middle(update)
    else:
        incorrect_updates.append(update)


# print(sum)

def fix_order(update, rules):
    while True:
        changed = False
        for x, y in rules:
            if x in update and y in update and update.index(x) > update.index(y):
                x_index = update.index(x)
                y_index = update.index(y)
                update[x_index], update[y_index] = update[y_index], update[x_index]
                changed = True
        if not changed:
            break
    return update


sum_correct = 0

sum_fixed = 0
for update in incorrect_updates:
    fixed_update = fix_order(update, rules)
    sum_fixed += get_middle(fixed_update)
    print(fixed_update)

print(sum_fixed)
