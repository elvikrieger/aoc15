with open("puzzle_input.txt") as f:
# with open("test.txt") as f:
    registers, program = f.read().split("\n\n")
    a, b, c = map(int, [reg.split(": ")[1] for reg in registers.strip().split("\n")])
    program = list(map(int, list(program.replace("Program: ", "").strip().split(","))))


class Computer:
    def __init__(self, a, b, c, instructions):
        self.a = a
        self.b = b
        self.c = c
        self.pointer = 0
        self.instructions = instructions
        self.opcodes = {
            0: self.adv,
            1: self.bxl,
            2: self.bst,
            3: self.jnz,
            4: self.bxc,
            5: self.out,
            6: self.bdv,
            7: self.cdv,
        }
        self.b_changes = 0

    def __repr__(self):
        return f"A: {self.a} B: {self.b} C: {self.c}"

    def get_operand_value(self, operand, ):
        if operand in [0, 1, 2, 3]:
            return operand
        elif operand == 4:
            return self.a
        elif operand == 5:
            return self.b
        elif operand == 6:
            return self.c

    def adv(self, operand):
        operand = self.get_operand_value(operand)
        self.a = self.a // (2 ** operand)

    def bxl(self, operand):
        self.b = self.b ^ operand

    def bst(self, operand):
        operand = self.get_operand_value(operand)
        self.b = operand % 8

    def jnz(self, operand):
        if self.a != 0:
            self.pointer = operand
        else:
            self.pointer += 2

    def bxc(self, operand):
        self.b = self.b ^ self.c

    def out(self, operand):
        operand = self.get_operand_value(operand)
        return operand % 8

    def bdv(self, operand):
        operand = self.get_operand_value(operand)
        self.b = self.a // (2 ** operand)

    def cdv(self, operand):
        operand = self.get_operand_value(operand)
        self.c = self.a // (2 ** operand)

    def compute(self):
        outputs = []
        while self.pointer < len(self.instructions):
            opcode = int(self.instructions[self.pointer])
            operand = int(self.instructions[self.pointer + 1])
            if opcode == 5:  # out
                value = self.out(operand)
                outputs.append(value)
            elif opcode in self.opcodes:
                self.opcodes[opcode](operand)
            else:
                raise ValueError("Invalid opcode")
            if opcode != 3:
                self.pointer += 2

        return ",".join(map(str, outputs))


computer = Computer(a, b, c, program)
result= computer.compute()
print(result)
assert result == "6,5,4,7,1,6,0,3,1"
print(program)
print(program[:1])
print(program[:3])