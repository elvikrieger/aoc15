from pprint import pprint


with open("test.txt") as f:
# with open("puzzle_input.txt") as f:
    conections = [ tuple(con.split("-")) for con in f.read().split()]

print(conections)


def pair_pcs(pc_pair:tuple):
    a,b = pc_pair
    if a in conections_dict:
        conections_dict[a].append(b)
    else:
        conections_dict[a] = [b]
    if b in conections_dict:
        conections_dict[b].append(a)
    else:
        conections_dict[b] = [a]


conections_dict = {}
for conection in conections:
    pair_pcs(conection)

pprint(conections_dict)

triangles = set()
for node, neighbors in conections_dict.items():
    for i, neighbor1 in enumerate(neighbors):
        for neighbor2 in neighbors[i + 1:]:
            if neighbor2 in conections_dict[neighbor1]:
                triangle = tuple(sorted([node, neighbor1, neighbor2]))
                triangles.add(triangle)

print(triangles)
result = set()
for triangle in triangles:
    for node in triangle:
        if node.startswith("t"):
            result.add(triangle)
            continue

print(len(result))
