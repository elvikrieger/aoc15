from functools import cache

with open("puzzle_input.txt") as f:
    # with open("test.txt") as f:
    patterns, designs = f.read().split("\n\n")
    patterns = patterns.strip().split(", ")
    designs = designs.strip().split()


@cache
def found_match(prefered_design):
    ways = 0
    if not prefered_design:
        return 1
    for pattern in patterns:
        if prefered_design.startswith(pattern):
            ways += found_match(prefered_design[len(pattern):])
    return ways


possible = 0
for design in designs:
    possible += found_match(design)
print(possible)
