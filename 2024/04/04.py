from pprint import pprint



def find_xmas(word_search):
    coords = [(0, 1), (1, 0), (1, 1), (1, -1), (0, -1), (-1, 0), (-1, -1), (-1, 1)]
    n = len(word_search)
    m = len(word_search[0])
    def prohledat_smer(x, y, dx, dy):
        stav = 0
        xmas_string = "XMAS"
        for index in range(len(xmas_string)):
            nx = x + index * dx
            ny = y + index *dy
            # když je větší než 0 a menší než délka řádku/sloupce A pokud odpovídá písmenu
            if 0 <= nx < n and 0 <= ny < m and word_search[nx][ny] == xmas_string[stav]:
                stav += 1
                if stav == len(xmas_string):
                    return True
            else:
                return False
        return False

    xmas_count = 0
    for i in range(n):
        for j in range(m):
            for dx, dy in coords:
                if prohledat_smer(i, j, dx, dy):
                    xmas_count += 1

    return xmas_count

#   M S    S S    M M   S M
#    A      A      A     A
#   M S    M M    S S   S M
def get_masks():
    masks = [
        # [   [" ", " ", " ", " "],
        #     [" ","M", " ", "S"],
        #     [" "," ", "A", " "],
        #     [" ","S", " ", "M"]
        # ],
        [[" ", " ", " ", " "],
            [" ","S", " ", "S"],
            [" "," ", "A", " "],
            [" ","S", " ", "S"]
        ],
        [[" ", " ", " ", " "],
            [" ","M", " ", "M"],
            [" "," ", "A", " "],
            [" ","S", " ", "S"]
        ],
        # [[" ", " ", " ", " "],
        #     [" ","S", " ", "M"],
        #     [" "," ", "A", " "],
        #     [" ","M", " ", "S"]
        # ]
    ]
    return masks

masks = get_masks()
def find_x_mas(word_search, masks):
    coords = [(1, 1), (1, -1), (-1, -1), (-1, 1)]  # 4 směry diagonály
    n = len(word_search)  # řádky
    m = len(word_search[0])  # sloupce
    xmas_count = 0
    for row in range(1, n - 1):
        for column in range(1, m - 1):
            if word_search[row][column] != "A":
                continue
            diagonal1 = []
            diagonal1 += word_search[row+1][column+1]
            diagonal1 += word_search[row-1][column-1]
            diagonal2 = []
            diagonal2 += word_search[row+1][column-1]
            diagonal2 += word_search[row-1][column+1]
            if sorted(diagonal1) == sorted(diagonal2) == ["M", "S"]:
                xmas_count += 1
    return xmas_count

word_search = []
with open("puzzle_input.txt") as f:
# with open("test.txt") as f:
    for line in f:
        line = [letter for letter in line.strip()]
        word_search.append(line)

        # print(line)

# print(find_xmas(word_search))
print(find_x_mas(word_search, masks))