from collections import defaultdict

wires = defaultdict(int)
definitions = {}

def split_wires_to_dict(line):
    wire, number = line.strip().split(": ")
    wires[wire] = int(number)

def split_instruction(line):
    wire_a, instruction, wire_b, _, output = line.split()
    definitions[output] = (wire_a, instruction, wire_b)

def evaluate_wire(wire):
    if wire in wires:
        return wires[wire]
    if wire in definitions:
        wire_a, instruction, wire_b = definitions[wire]
        val_a = evaluate_wire(wire_a) if wire_a in definitions or wire_a in wires else wires[wire_a]
        val_b = evaluate_wire(wire_b) if wire_b in definitions or wire_b in wires else wires[wire_b]
        if instruction == "AND":
            wires[wire] = val_a & val_b
        elif instruction == "OR":
            wires[wire] = val_a | val_b
        elif instruction == "XOR":
            wires[wire] = val_a ^ val_b
    return wires[wire]

# with open("test.txt") as f:
# with open("smalltest.txt") as f:
with open("puzzle_input.txxt") as f:
    inputs, gate_definitions = f.read().strip().split("\n\n")

for wire in inputs.split("\n"):
    split_wires_to_dict(wire)

for line in gate_definitions.split("\n"):
    split_instruction(line)

z_wires = sorted([wire for wire in definitions if wire.startswith("z")], key=lambda x: int(x[1:]))
z_wires.reverse()
result_binary = "".join(str(evaluate_wire(wire)) for wire in z_wires)
result_decimal = int(result_binary, 2)

print(result_binary)
print(result_decimal)
