from pprint import pprint
# with open("test.txt") as f:
with open("puzzle_input.txt") as f:
    warehouse, moves = f.read().split("\n\n")


warehouse = [list(line.strip()) for line in warehouse.split()]
moves = moves.replace("\n", "").strip()
assert "\n" not in moves

# print(f"{len(warehouse)=} {len(warehouse[0])=}\n {moves=}")

walls = set()
boxes = set()
robot = None
for r in range(len(warehouse)):
    for c in range(len(warehouse[0])):
        if warehouse[r][c] == "#":
            walls.add((r,c))
        elif warehouse[r][c] == "O":
            boxes.add((r,c))
        elif warehouse[r][c] == "@":
            robot = (r,c)
        elif warehouse[r][c] == ".":
            pass
        else:
            print(f"{walls=}\n {boxes=}\n {robot=}")
            print(warehouse[r][c])
            raise ValueError


moves = list(moves)

directions = {"^": (-1, 0), "v": (1, 0), "<": (0, -1), ">": (0, 1)}

def go_bot_go(warehouse, robot_position, boxes, moves):
    for move in moves:
        dr, dc = directions[move]
        rr, rc = robot_position
        nr, nc = rr + dr, rc + dc  # Nová pozice robota

        if not (0 <= nr < len(warehouse) and 0 <= nc < len(warehouse[0])) or warehouse[nr][nc] == '#':
            continue  # Robot se nepohne, jde na další krok

        if (nr, nc) in boxes:
            chain = [(nr, nc)]
            while True:
                br, bc = chain[-1][0] + dr, chain[-1][1] + dc
                if (br, bc) in walls or (br, bc) in chain:
                    chain = []  # nejde posunout bednu
                    break
                if (br, bc) in boxes:
                    chain.append((br, bc))
                else:
                    break
            if chain:
                for br, bc in reversed(chain):
                    boxes.remove((br, bc))
                    boxes.add((br + dr, bc + dc))
            else:
                continue

        robot_position = (nr, nc)

    return boxes


# print(len(warehouse), len(warehouse[0]))
after_boxes = go_bot_go(warehouse, robot, boxes, list(moves))
result = 0
for  box in after_boxes:
    result += 100*box[0] + box[1]

print(result)

yayyyyy = 1421727
assert result == yayyyyy