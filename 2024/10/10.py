from collections import deque
from pprint import pprint

with open("test2.txt") as f:
# with open("puzzle_input.txt") as f:
    mapa = [list(map(int, line.strip())) for line in f]

starts = []

for r in range(len(mapa)):
    for c in range(len(mapa[0])):
        if mapa[r][c] == 0:
            starts.append((r,c))

directions = [(-1, 0), (0, 1), (1, 0), (0, -1)]

def find_mountains(start, topo_map):
    directions = [(-1, 0), (0, 1), (1, 0), (0, -1)]

    to_visit = deque([start])
    visited = set()  # pozice kdejsme byli
    visited.add(start)
    reachable_nines = set() # pozice kde jsou hory
    while to_visit:

        current = to_visit.popleft()
        x, y = current
        current_height = topo_map[x][y]  # aktuální výška

        for dx, dy in directions:
            nx, ny = x+dx, y+dy
            if (0 <= nx < len(topo_map) and 0 <= ny < len(topo_map[0]) and topo_map[nx][ny] == current_height + 1):
                if topo_map[nx][ny] == 9:

                    reachable_nines.add((nx, ny))
                else:
                    to_visit.append((nx, ny))
                    visited.add((nx, ny))
    return len(reachable_nines)

trails = 0
for start in starts:
    x = find_mountains(start, mapa)
    trails += x


print(trails)

# part 2
def how_many_ways(start,topo_map):
    directions = [(-1, 0), (0, 1), (1, 0), (0, -1)]

    to_visit = deque([start])
    visited = set()  # pozice kdejsme byli
    visited.add(start)
    reachable_nines = set() # pozice kde jsou hory
    ways = 0
    while to_visit:

        current = to_visit.popleft()
        x, y = current
        current_height = topo_map[x][y]  # aktuální výška

        for dx, dy in directions:
            nx, ny = x+dx, y+dy
            if (0 <= nx < len(topo_map) and 0 <= ny < len(topo_map[0]) and topo_map[nx][ny] == current_height + 1):
                if topo_map[nx][ny] == 9:

                    reachable_nines.add((nx, ny))
                    ways+=1
                else:
                    to_visit.append((nx, ny))
                    visited.add((nx, ny))
    return ways

result = 0
for start in starts:
    x = how_many_ways(start, mapa)
    result += x

print(result)

