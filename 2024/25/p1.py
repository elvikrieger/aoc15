from pprint import pprint

def get_locks_and_keys(puzzle_list):
    keys = []
    locks = []
    for thing in puzzle_list:
        item = thing.split("\n")
        match item[0]:
            case "#####":
                locks.append(list(map(list, zip(*item[1:-1]))))
            case ".....":
                keys.append(list(map(list, zip(*item[1:-1]))))
    return keys, locks


with open("puzzle_input.txt") as f:
# with open("test.txt") as f:
    raw_locks_and_keys = f.read().split("\n\n")

keys, locks = get_locks_and_keys(raw_locks_and_keys)

def get_heights(key_lock:list):
    heights = []
    for thing in key_lock:
        heights.append(thing.count("#"))
    return heights

ok_pairs = 0
for key in keys:
    key_heights = get_heights(key)
    # print(key_heights)
    for lock in locks:
        lock_heights = get_heights(lock)
        for index, i in enumerate(key_heights):
            if i+lock_heights[index] >5:
                is_ok = False
                break
            else:
               is_ok = True
        if is_ok:
            ok_pairs +=1
            print("vvvv OK")
            # print(lock, key)
            print(lock_heights,key_heights)



print(ok_pairs)

