from collections import deque
from pprint import pprint

grid_size = 70
# grid_size = 7
grid = [["." for _ in range(grid_size + 1)] for _ in range(grid_size + 1)]

obstacles = []
other_bites = []
# max_bites = 12
max_bites = 1024
# with open("test.txt") as f:
with open("puzzle_input.txt") as f:
    bites = 0
    for line in f:
        c, r = list(map(int, line.strip().split(",")))
        if bites < max_bites:
            grid[r][c] = "#"
            obstacles.append((r, c))
            bites += 1

        else:
            other_bites.append((r, c))

for r in grid:
    print(" ".join(r))


def find_way(grid_size, end_point, obstacles):
    n = grid_size  # velikost gridu
    start = (0, 0)
    end = end_point
    to_check = deque([(start, 0, [start])])  # (pozice, počet kroků, cesta)
    checked = set()
    checked.add(start)

    directions = [(0, 1), (1, 0), (0, -1), (-1, 0)]

    while to_check:
        # print(to_check)
        (x, y), steps, path = to_check.popleft()

        if (x, y) == end:
            return steps, path

        for dx, dy in directions:
            nx, ny = x + dx, y + dy

            if 0 <= nx < n and 0 <= ny < n and (nx, ny) not in obstacles and (nx, ny) not in checked:
                checked.add((nx, ny))
                to_check.append(((nx, ny), steps + 1, path + [(nx, ny)]))

    return 0, 0


starting_point = (0, 0)
end_point = (70, 70)
grid_size = 71
for index, bite in enumerate(other_bites):
    obstacles.append(bite)
    min_steps, shortest_path = find_way(grid_size, end_point, obstacles)
    print(index)
    if min_steps == 0:
        print(f"{bite[1]},{bite[0]}")
        break
