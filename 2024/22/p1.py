


with open("test.txt") as f:
# with open("puzzle_input.txt") as f:
    numbers = list(map(int, f.read().strip().split()))

print(numbers)


def generate_next_secret(number):
    modulo = 16777216
    number = (number ^ (number * 64)) % modulo
    number = (number ^ (number // 32)) % modulo
    number = (number ^ (number * 2048)) % modulo
    return number

def get_it_2000times(number):
    for _ in range(2000):
        number = generate_next_secret(number)
    return number

def generate_prices(number, iterations):
    prices = []
    for _ in range(iterations):
        prices.append(number%10)
        number = generate_next_secret(number)
    return prices


def calculate_price_changes(prices):
    changes = []
    for i in range(1, len(prices)):
        changes.append(prices[i]-prices[i-1])
    return changes

test_numbers = [123]
numbers = test_numbers
for number in numbers:
    prices = generate_prices(number, 2000)
    print(number, "->", prices)
    changes = calculate_price_changes(prices)
    print(changes)
    sequences = []
    for i in range(len(changes) - 3):
        seq = changes[i:i+4]
        sequences.append(seq)
        print(seq)

print(sequences)

for monkey in numbers:
    for i in range(2000-4):
        last_change = sequences.pop()
        price_after_2000 = get_it_2000times(monkey) % 10





