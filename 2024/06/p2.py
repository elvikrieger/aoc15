from helpers import is_not_edge, can_move, move, rotate


with open("test.txt") as f:
# with open("puzzle_input.txt") as f:
    lab = [list(line.strip()) for line in f]

def get_lab(file_name):
    with open(file_name) as f:
        lab = [list(line.strip()) for line in f]
    return lab


actual_position = [0, 0]
for r in range(len(lab)):
    for c in range(len(lab[0])):
        if lab[r][c] == "^":
            actual_position = [r, c]
            break
orientation = (-1, 0)
# print(actual_position)

def go_guard(playground, position, directions):
    while is_not_edge(playground, position, directions):
        if can_move(playground, position, directions):
            playground, position = move(playground, position, directions)
        else:
            playground, directions = rotate(playground, position, directions)
    return playground

def go_part_two(playground, position, directions):
    visited= set()
    while is_not_edge(playground, position, directions):
        state = (tuple(position), directions)
        if state in visited:
            return True
        visited.add(state)
        if can_move(playground, position, directions):
            playground, position = move(playground, position, directions)
        else:
            playground, directions = rotate(playground, position, directions)
    return False

cycles = 0

_file = "test.txt"
_file = "puzzle_input.txt"
lab = get_lab(_file)
starting_position = [71, 59]
# starting_position = [6,4]
for r in range(len(lab)):
    for c in range(len(lab[0])):
        # starting_position = [6, 4]
        starting_position = [71, 59]
        if lab[r][c] in ["#", "^"]:
            continue
        test_lab = get_lab(_file)
        test_lab[r][c] = "#"
        if go_part_two(test_lab, starting_position, orientation):
            cycles += 1
            print("\n\n\n")


print(f"{cycles=}")

