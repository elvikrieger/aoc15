def is_not_edge(playground, position, direction):
    a, b = position
    dy, dx = direction
    rows = len(playground)
    columns = len(playground[0])
    if 0 <= a + dy < rows and 0 <= b + dx < columns:
        return True
    return False


def can_move(playground, position, direction):
    a, b = position
    dy, dx = direction
    try:
        if playground[a + dy][b + dx] != "#":
            return True
    except IndexError:
        pass
    return False


def is_cycling(playground, position, direction):
    path = {(-1, 0): "^",
            (0, 1): ">",
            (1, 0): "v",
            (0, -1): "<"}
    a, b = position
    dy, dx = direction
    if playground[a + dy][b + dx] == path[direction]:
        return True
    return False

def move(playground, position, direction):
    path = {(-1, 0): "^",
            (0, 1): ">",
            (1, 0): "v",
            (0, -1): "<"}
    if playground[position[0]][position[1]] != "+":
        playground[position[0]][position[1]] = path[direction]
    position[0] += direction[0]
    position[1] += direction[1]
    return playground, position


def rotate(playground, position, direction):
    rotate = {(-1, 0): (0, 1),
              (0, 1): (1, 0),
              (1, 0): (0, -1),
              (0, -1): (-1, 0)}
    playground[position[0]][position[1]] = "+"
    return playground, rotate[direction]

def print_lab(playground):
    for row in playground:
        print(" ".join(row))
