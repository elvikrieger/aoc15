# with open("test.txt") as f:
with open("puzzle_input.txt") as f:
    lab = [list(line.strip()) for line in f]

# radky nahoru -1, dolu +1
# sloupce doleva -1, doprava +1

directions = [(-1, 0), (0, 1), (1, 0), (0, -1)]
position = 0
for r in range(0, len(lab) - 1):
    for c in range(0, len(lab[0])):
        if lab[r][c] == "^":
            position = [r, c]

orientation = (-1, 0)
rotate = {(-1, 0): (0, 1),
          (0, 1): (1, 0),
          (1, 0): (0, -1),
          (0, -1): (-1, 0)
          }

in_grid = True
while in_grid:
    try:
        a, b = position
        lab[a][b] = "O"
        r, c = orientation
        if a + r >= 0 and b + c >= 0:
            if lab[a + r][b + c] != "#":
                position[0] += orientation[0]
                position[1] += orientation[1]
            else:
                orientation = rotate[orientation]
        else:
            in_grid = False
    except IndexError:
        in_grid = False

visited = 0
for r in range(0, len(lab)):
    for c in range(0, len(lab[r])):
        if lab[r][c] == "O":
            visited += 1
print(visited)
