def split_line(line):
    a,b = line.strip().split()
    return a,b

left_list = []
right_list =     []
differences = []

with open("puzzle_input.txt") as f:
# with open("test01.txt") as f:
    for line in f:
        a,b = split_line(line)
        left_list.append(int(a))
        right_list.append(int(b))

# PART ONE
# while len(left_list)>0:
#     right_min = min(right_list)
#     left_min = min(left_list)
#     differences.append(abs(left_min - right_min))
#     left_list.remove(left_min)
#     right_list.remove(right_min)
# print(sum(differences))

# PART TWO
similarities = []
for number in left_list:
    count_in_right = right_list.count(number)
    similarities.extend([count_in_right*number])
print(sum(similarities))