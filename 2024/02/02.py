from stringprep import in_table_c7


def split_line(line):
    numbers = line.strip().split()
    return [int(number) for number in numbers]

safe = 0
# with open("test.txt") as f:
with open("puzzle_input.txt") as f:
    for line in f:
        numbers = split_line(line)

        diff_list = []
        for index, x in enumerate(numbers[:-1]):
            y = numbers[index + 1]
            if 1 <= abs(x - y) <= 3:
                diff_list.append(x - y)
            else:
                break
        if len(diff_list) == len(numbers) - 1:
            if all(z > 0 for z in diff_list) or all(z < 0 for z in diff_list):
                safe += 1

print(safe)



# part II
def is_safe(line_list):
    diff_list = []
    for index, x in enumerate(line_list[:-1]):
        y = line_list[index + 1]
        if 1 <= abs(x - y) <= 3:
            diff_list.append(x - y)
        else:
            return False
    if all(z > 0 for z in diff_list) or all(z < 0 for z in diff_list):
        return True
    else:
        return False


safe = 0
# with open("test.txt") as f:
with open("puzzle_input.txt") as f:
    for line in f:
        numbers = split_line(line)

        if is_safe(numbers):
            safe +=1
        else:
            safe_after_removal = False
            for i in range(len(numbers)):
                new_list = numbers[:i] + numbers[i + 1:]
                if is_safe(new_list):
                    safe_after_removal = True
                    safe += 1
                    break

print(safe)