from itertools import batched
from pprint import pprint

test = "2333133121414131402"
with open("puzzle_input.txt") as f:
    puzzle = f.read()

# puzzle = test
puzzle_batched = list(batched(puzzle, 2))

f_id = 0

ids = {}
ids_names = []
dsk = 0
dots = []
for tuple in puzzle_batched[:-1]:
    file, space = tuple
    if file:
        ids[f_id] = [dsk, int(file)]
        ids_names.append(f_id)
        x = [f_id] * int(file)
        dsk += len(x)

    if space:
        x = ["." for _ in range(int(space))]
        if x != []:
            dots.append([dsk, len(x)])
            dsk += len(x)
    f_id += 1

last = [f_id] * int(puzzle_batched[-1][-1])
ids[f_id] = [dsk, int(puzzle_batched[-1][-1])]
ids_names.append(f_id)
ids_names.reverse()

for id in ids_names:
    id_index, id_len = ids[id]
    for index, dot in enumerate(dots):
        dot_index, dot_len = dot
        if id_len <= dot_len and dot_index < id_index:
            ids[id][0] = dot_index  # id index
            len_diff = dot_len - id_len

            dots[index][0] += id_len
            dots[index][1] -= id_len
            break

result = 0
for id in ids:
    len_id = ids[id][1]
    for i in range(len_id):
        result += id*(ids[id][0]+i)

print(result)