from itertools import batched

test = "2333133121414131402"

with open("puzzle_input.txt") as f:
    puzzle = f.read()

# puzzle = test
puzzle_batched = list(batched(puzzle, 2))
id = 0
disk = []
for tuple in puzzle_batched[:-1]:
    file, space = tuple
    if file:
        disk.extend([str(id) for i in range(int(file))])
    if space:
        disk.extend(["." for i in range(int(space))])
    id +=1
disk.extend([str(id) for i in range(int(puzzle_batched[-1][-1]))])

dots = disk.count(".")

nums = []
for i in disk:
    try:
        nums.append(int(i))
    except ValueError:
        pass
rearanged_disk = list(disk)

rearanged_disk = [str(i) for i in rearanged_disk]

for index, n in enumerate(rearanged_disk):
    if n == "." and nums!=[]:
        rearanged_disk[index] = nums[-1]
        nums = nums[:-1]


for i in range(1,dots+1):
    rearanged_disk[-i] = "."

rearanged_disk = [str(i) for i in rearanged_disk]
result = 0
for index, file in enumerate(rearanged_disk):
    if file !=".":
        result += index*int(file)
    else:
        pass


print(result)
