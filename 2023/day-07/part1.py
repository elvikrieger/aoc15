# Every hand is exactly one type. From strongest to weakest, they are:
#     FIVE OF A KIND, where all five cards have the same label: AAAAA
#     FOUR OF A KIND, where four cards have the same label and one card has a different label: AA8AA
#     FULL HOUSE, where three cards have the same label, and the remaining two cards share a different label: 23332
#     THREE OF A KIND, where three cards have the same label, and the remaining two cards are each different from any other card in the hand: TTT98
#     TWO PAIR, where two cards share one label, two other cards share a second label, and the remaining card has a third label: 23432
#     ONE PAIR, where two cards share one label, and the other three cards have a different label from the pair and each other: A23A4
#     HIH CARD, where all cards' labels are distinct: 23456
# Hands are primarily ordered based on type; for example, every full house is stronger than any three of a kind.

# If two hands have the same type, a second ordering rule takes effect. 
# Start by comparing the first card in each hand. 
# If these cards are different, the hand with the stronger first card is considered stronger. 
# If the first card in each hand have the same label, however, then move on to considering the second card in each hand. 
# If they differ, the hand with the higher second card wins; otherwise, continue with the third card in each hand, then the fourth, then the fifth.


card_order = ["A", "K", "Q", "J", "T", "9", "8", "7", "6", "5", "4", "3", "2"]

def split_hand_and_bid(line):
    hand, bid = line.split(" ", 1)

    return (hand, int(bid))



hands_and_bids = []

with open("test.txt") as f:
# with open("puzzle_input.txt") as f:
    for line in f:
        hand_bid = split_hand_and_bid(line.strip())
        hands_and_bids.append(hand_bid)

idk = zip(hands_and_bids)

print(idk)
