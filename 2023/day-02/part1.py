# The Elf would first like to know which games would have been possible if the bag contained only 12 red cubes, 13 green cubes, and 14 blue cubes?
def split_line_to_game_and_turns(line):
    game, turns = line.split(": ", 1)

    return game, turns


def split_turns(turn_str):
    turns = list(turn_str.split("; "))
    return turns


def split_turns_to_dicts(turns_list):
    game_turns_list = []  # list který bude ukládat kostky v jednotlivých tazích
    for turn in turns_list:
        turn_dict = {}
        cube_set = turn.split(", ")
        for cube in cube_set:
            number, colour = cube.split(" ")
            turn_dict[colour] = int(number)
        game_turns_list.append(turn_dict)
    return game_turns_list


red_cubes = 12
green_cubes = 13
blue_cubes = 14


ok_games = []
games_dict = {}
# with open("test.txt") as f:
with open("puzzle_input.txt") as f:
    for line in f:
        game, turns = split_line_to_game_and_turns(line.strip())
        games_dict[game] = split_turns_to_dicts(split_turns(turns))


for game in games_dict:
    game_check = []
    for turn in games_dict[game]:
        for cube_set in turn:
            if cube_set == "blue":
                if blue_cubes < turn[cube_set]:
                    game_check.append("nope")

            if cube_set == "red":
                if red_cubes < turn[cube_set]:
                    game_check.append("nope")
            if cube_set == "green":
                if green_cubes < turn[cube_set]:
                    game_check.append("nope")
    if "nope" not in game_check:
        ok_games.append(game)


sum_ok_games = 0
for game in ok_games:
    game, number = game.split(" ")
    sum_ok_games += int(number)

print(sum_ok_games)
