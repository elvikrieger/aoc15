turns_list = ["8 green, 6 blue, 20 red", "5 blue, 4 red, 13 green", "5 green, 1 red"]

turns_list_dict = [{"green": 8, "blue": 6, "red": 20}, {"blue": 5, "red": 4, "green": 13}, {"green": 5, "red": 1}]

def split_turns_to_dicts(turns_list):
    game_turns_list = [] # list který bude ukládat kostky v jednotlivých tazích
    for turn in turns_list:
        turn_dict = {}
        cube_set = turn.split(", ")
        for cube in cube_set:
            number, colour = cube.split(" ")
            turn_dict[colour] = int(number)
        game_turns_list.append(turn_dict)
    return game_turns_list


print(split_turns_to_dicts(turns_list))


