# As you continue your walk, the Elf poses a second question: in each game you played,
# what is the fewest number of cubes of each color that could have been in the bag to make the game possible?

# The power of a set of cubes is equal to the numbers of red, green, and blue cubes multiplied together.
# The power of the minimum set of cubes in game 1 is 48. In games 2-5 it was 12, 1560, 630, and 36, respectively.
# Adding up these five powers produces the sum 2286.


def split_line_to_game_and_turns(line):
    game, turns = line.split(": ", 1)

    return game, turns


def split_turns(turn_str):
    turns = list(turn_str.split("; "))
    return turns


def split_turns_to_dicts(turns_list):
    game_turns_list = []  # list který bude ukládat kostky v jednotlivých tazích
    for turn in turns_list:
        turn_dict = {}
        cube_set = turn.split(", ")
        for cube in cube_set:
            number, colour = cube.split(" ")
            turn_dict[colour] = int(number)
        game_turns_list.append(turn_dict)
    return game_turns_list


#   Game X: [{"green": 8, "blue": 6, "red": 20}, {"blue": 5, "red": 4, "green": 13}, {"green": 5, "red": 1}]


games_dict = {}
# with open("test.txt") as f:
with open("puzzle_input.txt") as f:
    for line in f:
        game, turns = split_line_to_game_and_turns(line.strip())
        games_dict[game] = split_turns_to_dicts(split_turns(turns))



sum_cube_power = 0
for game in games_dict:
    green_cubes = 0
    blue_cubes = 0
    red_cubes = 0
    for turn in games_dict[game]:
        for cube_set in turn:
            if cube_set == "blue":
                if blue_cubes < turn[cube_set]:
                    blue_cubes = turn[cube_set]
            if cube_set == "red":
                if red_cubes < turn[cube_set]:
                    red_cubes = turn[cube_set]
            if cube_set == "green":
                if green_cubes < turn[cube_set]:
                    green_cubes = turn[cube_set]
    sum_cube_power += blue_cubes*red_cubes*green_cubes
print(sum_cube_power)
