# As far as the Elf has been able to figure out, you have to figure out which of the numbers you have appear in the list of winning numbers.
# The first match makes the card worth one point and each match after the first doubles the point value of that card.
# Take a seat in the large pile of colorful cards. How many points are they worth in total?

def remove_spaces(name):
    while " " in name:
        name = name.replace(" ", "")
    return name

def split_line_to_card_and_numbers(line):
    card, numbers = line.split(": ", 1)
    card = remove_spaces(card)
    return card, numbers


def separate_numbers(numbers):
    numbers_list = numbers.split()
    return numbers_list


def split_numbers(numbers_str):
    winning_numbers, player_numbers = numbers_str.split(" | ")
    winning_numbers = separate_numbers(winning_numbers)
    player_numbers = separate_numbers(player_numbers)
    return winning_numbers, player_numbers




cards_dict = {}
cards_list = []
# with open("test.txt") as f:
with open("puzzle_input.txt") as f:
    for line in f:
        card, numbers = split_line_to_card_and_numbers(line.strip())
        winning_numbers, player_numbers = split_numbers(numbers)
        cards_list.append(card)
        for index, number in enumerate(winning_numbers):
            winning_numbers[index] = int(number)
        for index, number in enumerate(player_numbers):
            player_numbers[index] = int(number)
        cards_dict[card] = [winning_numbers, player_numbers]
# print(cards_dict)

cards_total = 0
how_many_does_card_add_dict = {}
new_added_cards = []
for index, card in enumerate(cards_list):
    winning_numbers_in_card = []
    for number in cards_dict[card][1]:
        if number in cards_dict[card][0]:
            winning_numbers_in_card.append(number)
        how_many_does_card_add_dict[card] =[[len(winning_numbers_in_card)],["Card"+ str(i) for i in range(index+2, len(winning_numbers_in_card)+index+2)]]

# print(how_many_does_card_add_dict)
# print(cards_list)



for card in cards_list:
    for one_card in how_many_does_card_add_dict[card][1]:
        cards_list.append(one_card)
    cards_total +=1

print(cards_total)
