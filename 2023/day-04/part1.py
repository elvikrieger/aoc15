# As far as the Elf has been able to figure out, you have to figure out which of the numbers you have appear in the list of winning numbers.
# The first match makes the card worth one point and each match after the first doubles the point value of that card.
# Take a seat in the large pile of colorful cards. How many points are they worth in total?


def split_line_to_card_and_numbers(line):
    card, numbers = line.split(": ", 1)

    return card, numbers


def separate_numbers(numbers):
    numbers_list = numbers.split()
    return numbers_list


def split_numbers(numbers_str):
    winning_numbers, player_numbers = numbers_str.split(" | ")
    winning_numbers = separate_numbers(winning_numbers)
    player_numbers = separate_numbers(player_numbers)
    return winning_numbers, player_numbers


cards_dict = {}
# with open("test.txt") as f:
with open("puzzle_input.txt") as f:
    for line in f:
        card, numbers = split_line_to_card_and_numbers(line.strip())
        winning_numbers, player_numbers = split_numbers(numbers)
        for index, number in enumerate(winning_numbers):
            winning_numbers[index] = int(number)
        for index, number in enumerate(player_numbers):
            player_numbers[index] = int(number)
        cards_dict[card] = [winning_numbers, player_numbers]

cards_worth_sum = 0
for card in cards_dict:
    card_worth = 0
    winning_numbers_in_card = []
    for number in cards_dict[card][1]:
        if number in cards_dict[card][0]:
            winning_numbers_in_card.append(number)
    if len(winning_numbers_in_card) > 0:
        card_worth = 2 ** (len(winning_numbers_in_card) - 1)
    cards_worth_sum += card_worth

print(cards_worth_sum)
