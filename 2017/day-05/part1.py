instructions_list = []

with open("puzzle_input.txt") as f:
# with open("test.txt") as f:
    for line in f:
        instruction = line.strip()
        instructions_list.append(int(instruction))


steps = 0
class Panacek:
    def __init__(self):
        self.poloha = 0
        self.pocet_skoku = 0

    def jump(self):
        kam = self.poloha + instructions_list[self.poloha]
        instructions_list[self.poloha] += 1
        self.poloha = kam
        self.pocet_skoku += 1



panacek = Panacek()

while True:
    try:
        panacek.jump()
        
    except IndexError:
        print(panacek.pocet_skoku)
        break
