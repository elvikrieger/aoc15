start_banks = "4	10	4	1	8	4	9	14	5	1	14	15	0	15	3	5"
# start_banks = "0 2 7 0"


banks = start_banks.split()
banks = [int(x) for x in banks]

banks_state = []
cycle_number = 0

# print(banks_state)
while tuple(banks) not in banks_state:
    banks_state.append(tuple(banks))
    cycle_number += 1
    to_be_redistributed = max(banks)
    where_from = banks.index(to_be_redistributed)
    banks[where_from] = 0

    for _ in range(to_be_redistributed):
        banks[(where_from + 1) % len(banks)] += 1
        where_from = (where_from + 1) % len(banks)
    
    # part 2
    if tuple(banks) in banks_state:
        # print(banks_state.index(tuple(banks)))
        print(cycle_number - (banks_state.index(tuple(banks))))
    # print(banks_state)


print(cycle_number)
