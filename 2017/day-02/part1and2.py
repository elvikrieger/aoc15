def split_to_list(numbers):
    list_of_numbers = list(numbers.split())
    list_of_numbers = [int(i) for i in list_of_numbers]
    return list_of_numbers

def get_difference(list_of_numbers):
    list_of_numbers.sort()
    return list_of_numbers[-1] - list_of_numbers[0]

def try_to_divide(list_of_numbers):
    line_sum = 0
    for i in list_of_numbers:
        for j in list_of_numbers:
            if i != j and i % j == 0:
                line_sum = i / j
    return line_sum
                
                

sum_total = 0
# with open("test.txt") as f:
with open("puzzle_input.txt") as f:
    for line in f:
        line_numbers = split_to_list(line)
        sum_total += try_to_divide(line_numbers)

print(sum_total)