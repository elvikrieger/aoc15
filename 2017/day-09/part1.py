clearing_puzzle_input = ""
i = 0

with open("puzzle_input.txt") as f:
# with open("test.txt") as f:
    line = f.readline()
    while i < len(line):
        if line[i] == "<":
            i +=1
            while line[i] != ">":
                if line[i] == "!":
                    i +=2
                else:
                    i += 1
        elif line[i] == "{" or line[i] == "}" :
            clearing_puzzle_input += line[i]
            i += 1
        else:
            i +=1

leve = clearing_puzzle_input.count("{")
prave = clearing_puzzle_input.count("}")


pocitatko = 0
result = 0 
for zavorka in clearing_puzzle_input:
    if zavorka == "{":
        pocitatko += 1
    elif zavorka == "}":
        result += pocitatko
        pocitatko -= 1


print(f"{leve=}, {prave=}")
print(result)