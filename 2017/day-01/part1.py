total = 0
# with open("test.txt") as f:
with open("puzzle_input.txt") as f:
    for line in f:
        for index, number in enumerate(line):
            try:
                if number == line[index+1]:
                    total += int(number)
            except IndexError:
                if line[-1] == line[0]:
                    total += int(number)
print(total)