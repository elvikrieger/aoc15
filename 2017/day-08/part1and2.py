# Each instruction consists of several parts: the register to modify,
# whether to increase or decrease that register's value,
# the amount by which to increase or decrease it,
# and a condition. If the condition fails, skip the instruction without modifying the register.
# The registers all start at 0.


splitted_lines = []


class Pokladna:
    def __init__(self, line_list):
        self.promenne_dict = {}
        for line in splitted_lines:
            if line[0] not in self.promenne_dict:
                self.promenne_dict[line[0]] = 0
        self.line_list = line_list
        self.nejvyssi_hodnota = 0

    def increase_register(self, line):
        if line[5] == ">":
            if self.promenne_dict[line[4]] > int(line[6]):
                self.promenne_dict[line[0]] += int(line[2])
        elif line[5] == "<":
            if self.promenne_dict[line[4]] < int(line[6]):
                self.promenne_dict[line[0]] += int(line[2])
        elif line[5] == ">=":
            if self.promenne_dict[line[4]] >= int(line[6]):
                self.promenne_dict[line[0]] += int(line[2])
        elif line[5] == "<=":
            if self.promenne_dict[line[4]] <= int(line[6]):
                self.promenne_dict[line[0]] += int(line[2])
        elif line[5] == "==":
            if self.promenne_dict[line[4]] == int(line[6]):
                self.promenne_dict[line[0]] += int(line[2])
        elif line[5] == "!=":
            if self.promenne_dict[line[4]] != int(line[6]):
                self.promenne_dict[line[0]] += int(line[2])
        if self.promenne_dict[line[0]] > self.nejvyssi_hodnota:
            self.nejvyssi_hodnota = self.promenne_dict[line[0]]
    def decrease_register(self, line):
        if line[5] == ">":
            if self.promenne_dict[line[4]] > int(line[6]):
                self.promenne_dict[line[0]] -= int(line[2])
        elif line[5] == "<":
            if self.promenne_dict[line[4]] < int(line[6]):
                self.promenne_dict[line[0]] -= int(line[2])
        elif line[5] == ">=":
            if self.promenne_dict[line[4]] >= int(line[6]):
                self.promenne_dict[line[0]] -= int(line[2])
        elif line[5] == "<=":
            if self.promenne_dict[line[4]] <= int(line[6]):
                self.promenne_dict[line[0]] -= int(line[2])
        elif line[5] == "==":
            if self.promenne_dict[line[4]] == int(line[6]):
                self.promenne_dict[line[0]] -= int(line[2])
        elif line[5] == "!=":
            if self.promenne_dict[line[4]] != int(line[6]):
                self.promenne_dict[line[0]] -= int(line[2])
        if self.promenne_dict[line[0]] > self.nejvyssi_hodnota:
            self.nejvyssi_hodnota = self.promenne_dict[line[0]]

    def najdi_min(self):
        return self.promenne_dict[min(self.promenne_dict, key=self.promenne_dict.get)]

    def najdi_max(self):
        return self.promenne_dict[max(self.promenne_dict, key=self.promenne_dict.get)]


# with open("test_input.txt") as f:
with open("puzzle_input.txt") as f:
    for line in f:
        splitted_lines.append(line.strip().split())


# print(splitted_lines)


pokladna = Pokladna(splitted_lines)
# pokladna.ukaz_co_mas()
for line in pokladna.line_list:
    if line[1] == "inc":
        pokladna.increase_register(line)
    else:
        pokladna.decrease_register(line)

print(pokladna.nejvyssi_hodnota)
