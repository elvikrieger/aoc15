
puzzle_input = 277678
# puzzle_input = 1024


cislo_na_diagonale = 0

licha_cisla = [x for x in range(1, 600) if x % 2 > 0]
prvni_vetsi_z_diagonaly = []
# print(licha_cisla)

vzdalenost_ke_stredu = 0

while len(prvni_vetsi_z_diagonaly) < 1:
    for odmocnina_z_diagonaly in licha_cisla:
        cislo_na_diagonale = odmocnina_z_diagonaly * odmocnina_z_diagonaly
        if cislo_na_diagonale > puzzle_input and len(prvni_vetsi_z_diagonaly) < 1:
            prvni_vetsi_z_diagonaly.append(cislo_na_diagonale)
            delka_strany = odmocnina_z_diagonaly-1
            vzdalenost_ke_stredu += licha_cisla.index(delka_strany +1)

# print("vzdalenost ke stredu index v lichych cislech", licha_cisla.index(vzdalenost_ke_stredu))
odecitame_smerem_k_inputu = prvni_vetsi_z_diagonaly[0]

while odecitame_smerem_k_inputu > puzzle_input:
    odecitame_smerem_k_inputu -= delka_strany
    # print(kolik_stran_zpet)

stred_strany = int((odecitame_smerem_k_inputu+delka_strany+odecitame_smerem_k_inputu)/2)
if stred_strany > puzzle_input:
    while stred_strany > puzzle_input:
        stred_strany -=1
        vzdalenost_ke_stredu +=1
else:
    while stred_strany < puzzle_input:
        stred_strany +=1
        vzdalenost_ke_stredu +=1

print("vysledek",vzdalenost_ke_stredu)







