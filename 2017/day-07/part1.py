def split_to_name_and_rest(line_to_split):
    name, what_it_holds = line_to_split.split(" -> ")
    return name, what_it_holds

def split_name_and_weight(name_of_thingie):
    name, weight = name_of_thingie.split(" ")
    return name, weight

def split_holded_thingies(holded_thingies):
    holded_thingies_list = holded_thingies.split(", ")
    return holded_thingies_list


thingies_dict = {}
keys_list = []

with open("test.txt") as f:
# with open("puzzle_input.txt") as f:
    for line in f:
        try: 
            name, holds = split_to_name_and_rest(line.strip())
            thingies_dict[name] = split_holded_thingies(holds)
            name, weight = split_name_and_weight(name)
            keys_list.append(name)
        except:
            pass
        
print(thingies_dict)
while len(keys_list) > 1:
    for key_from_list in keys_list:
        for key in thingies_dict:
            if key_from_list in thingies_dict[key]:
                keys_list.remove(key_from_list)


print(keys_list)