def split_to_name_and_rest(line_to_split):
    try:
        name, what_it_holds = line_to_split.split(" -> ")
    except ValueError:
        name = line_to_split
        what_it_holds = []
    return name, what_it_holds


def split_name_and_weight(name_of_thingie):
    name, weight = name_of_thingie.split(" ")
    return name, weight


def split_holded_thingies(holded_thingies):
    holded_thingies_list = holded_thingies.split(", ")
    return holded_thingies_list


def sum_weight_for_disc_and_his_above_tier(disc):
    weights_of_above_discs = 0
    weights_of_above_discs += weight_dict[disc]
    for above_disc in dict_of_discs[disc]:
        weights_of_above_discs += weight_dict[above_disc]
    return weights_of_above_discs


def find_if_different(disc):
    weight_list_for_disc = []
    for above_disc in dict_of_discs[disc]:
        weight_list_for_disc.append(sum_weight_for_disc_and_his_above_tier(above_disc))
    if len(set(weight_list_for_disc)) > 1:
        return False
    else:
        return True


def find_where_bad():
    for disc in dict_of_discs:
        if find_if_different(disc) == False:
            return disc
        else:
            pass


class DiscTree:
    def __init__(self, name):
        self.name = name
        self.weight = weight_dict[name]
        self.children = dict_of_discs[self.name]
        for disc in dict_of_discs:
            if self.name in dict_of_discs[disc]:
                self.parent = disc
            else:
                self.parent = self.name

    def weights_of_self_and_above_discs(self):
        weights_of_above_discs = 0
        for above_disc in self.children:
            weights_of_above_discs += above_disc.weights_of_above_discs + weight_dict[above_disc] + self.weight
        return weights_of_above_discs


dict_of_discs = {}
weight_dict = {}
keys_list = []
list_of_discs = []

with open("test.txt") as f:
    # with open("puzzle_input.txt") as f:
    for line in f:
        name_weight, holds = split_to_name_and_rest(line.strip())
        name, weight = split_name_and_weight(name_weight)
        dict_of_discs[name] = holds
        if len(dict_of_discs[name]) > 1:
            dict_of_discs[name] = split_holded_thingies(holds)
        weight_dict[name] = int(weight[1:-1])
        keys_list.append(name)
        list_of_discs.append(name)

# print(find_if_different("vbsfwqh"))
# print(find_where_bad())

disc1 = Disc("tknk")
print(disc1.name, disc1.children, disc1.weight, disc1.parent)

print(disc1.weights_of_self_and_above_discs())
