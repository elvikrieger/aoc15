result = 0
with open("puzzle_input.txt") as f:
    for line in f:
        line = line.strip()
        result += int(line)

print(result)