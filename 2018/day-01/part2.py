result = 0
result_list = []
changes = []
with open("puzzle_input.txt") as f:
# with open("test_input.txt") as f:
    for line in f:
        line = line.strip()
        changes.append(int(line))

final_result = None
# changes = change*100
while final_result == None:
    for change in changes:
        if result not in result_list: 
            result_list.append(result)
            result += int(change)
            # print(result)
        else:
            final_result = result

print(f"{result=}")

# print(len(result_list))
# print(len(set(result_list)))