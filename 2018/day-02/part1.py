contains_exactly_twice = 0
contains_exactly_three_times = 0

with open("puzzle_input.txt") as f:
# with open("test.txt") as f:
    for line in f:
        # print(line.strip())
        letters = []
        double = False
        tripple = False
        for letter in line.strip():
            if letter not in letters:
                letters.append(letter)
        # print(letters)
        for letter in letters:
            if line.count(letter) == 2 and not double:
                contains_exactly_twice += 1
                double = True
                # print(line, letter, "2")
            elif line.count(letter) == 3 and not tripple:
                contains_exactly_three_times += 1
                tripple = True
                # print(line, letter, "3")
            else:
                continue
result = contains_exactly_twice * contains_exactly_three_times

print(result)