def compare_boxes(first_box, second_box):
    if first_box !=second_box:
        compare_letters = []
        for i in range(len(first_box)):
            if first_box[i] == second_box[i]:
                compare_letters.append("True")
            else:
                compare_letters.append("False")
                if compare_letters.count("False")> 1:
                    break
        if compare_letters.count("False") < 2:
            return compare_letters.index("False")
        else:
            return "Nope"
    else:
        return "Nope"

boxes = []

with open("puzzle_input.txt") as f:
# with open("test2.txt") as f:
    for line in f:
        boxes.append(line.strip())


for second_box in boxes:
    for box in boxes:
        result = compare_boxes(box, second_box)
        if result != "Nope":
            print(box[:15] + box[16:])
            break
        




# print(decomposed_boxes)