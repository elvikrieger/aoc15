# The design document gives the side lengths of each triangle it describes, 
# but... 5 10 25? Some of these aren't triangles. You can't help but mark the impossible ones.
# In a valid triangle, the sum of any two sides must be larger than the remaining side. 
# For example, the "triangle" given above is impossible, because 5 + 10 is not larger than 25.
# In your puzzle input, how many of the listed triangles are possible?

def split_numbers(line):
    s1, s2, s3 = line.split()
    return (int(s1), int(s2), int(s3))

def is_triangle(trojice):
    if trojice[0] + trojice[1] > trojice[2] and trojice[0] + trojice[2] > trojice[1] and trojice[1] + trojice[2] > trojice[0]:
        return True
    else:
        return False

maybe_triangles = []
number_of_triangles = 0
with open("puzzle_input.txt") as f:
    for line in f:
        maybe_triangles.append(split_numbers(line[:-1]))


print(maybe_triangles)
for trojice in maybe_triangles:
    if is_triangle(trojice) == True:
        number_of_triangles +=1

print(number_of_triangles)


