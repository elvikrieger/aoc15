# The design document gives the side lengths of each triangle it describes, 
# but... 5 10 25? Some of these aren't triangles. You can't help but mark the impossible ones.
# In a valid triangle, the sum of any two sides must be larger than the remaining side. 
# For example, the "triangle" given above is impossible, because 5 + 10 is not larger than 25.
# In your puzzle input, how many of the listed triangles are possible?

# Now that you've helpfully marked up their design documents, 
# it occurs to you that triangles are specified in groups of three vertically. 
# Each set of three numbers in a column specifies a triangle. Rows are unrelated.

def split_numbers(line):
    s1, s2, s3 = line.split()
    return [int(s1), int(s2), int(s3)]

def is_triangle(trojice):
    if int(trojice[0]) + int(trojice[1]) > int(trojice[2]) and int(trojice[0]) + int(trojice[2]) > int(trojice[1]) and int(trojice[1]) + int(trojice[2]) > int(trojice[0]):
        return True
    else:
        return False

maybe_triangles = []
number_of_triangles = 0
with open("puzzle_input.txt") as f:
    while True:
        try:
            l1 = next(f).strip().split()
            l2 = next(f).strip().split()
            l3 = next(f).strip().split()

            d = list(zip(l1, l2, l3))
            
            # print(l1, "|", l2, "|", l3)
            for trojice in d:
                if is_triangle(trojice) == True:
                    number_of_triangles += 1
                          
       
        except StopIteration:
            break


print(number_of_triangles)



