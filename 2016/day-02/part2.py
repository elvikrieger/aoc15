# Each button to be pressed can be found by starting on the previous button and 
# moving to adjacent buttons on the keypad: U moves up, D moves down, L moves left, and R moves right
# Each line of instructions corresponds to one button, starting at the previous button (or, for the first line, the "5" button);
# press whatever button you're on at the end of each line. If a move doesn't lead to a button, ignore it.

def up(position):
    if position == "1":
        return position
    elif position == "2":
        return position
    elif position == "3":
        position = "1"
        return position
    elif position == "4":
        return position
    elif position == "5":
        return position
    elif position == "6":
        position = "2"
        return position
    elif position == "7":
        position = "3"
        return position
    elif position == "8":
        position = "4"
        return position
    elif position == "9":
        return position
    elif position == "A":
        position = "6"
        return position
    elif position == "B":
        position = "7"
        return position
    elif position == "C":
        position = "8"
        return position
    elif position == "D":
        position = "B"
        return position
    
def down(position):
    if position == "1":
        position = "3"
        return position
    elif position == "2":
        position = "6"
        return position
    elif position == "3":
        position = "7"
        return position
    elif position == "4":
        position = "8"
        return position
    elif position == "5":
        return position
    elif position == "6":
        position = "A"
        return position
    elif position == "7":
        position = "B"
        return position
    elif position == "8":
        position = "C"
        return position
    elif position == "9":
        return position
    elif position == "A":
        return position
    elif position == "B":
        position = "D"
        return position
    elif position == "C":
        return position
    elif position == "D":
        return position

def left(position):
    if position == "1":
        return position
    elif position == "2":
        return position
    elif position == "3":
        position = "2"
        return position
    elif position == "4":
        position = "3"
        return position
    elif position == "5":
        return position
    elif position == "6":
        position = "5"
        return position
    elif position == "7":
        position = "6"
        return position
    elif position == "8":
        position = "7"
        return position
    elif position == "9":
        position = "8"
        return position
    elif position == "A":
        return position
    elif position == "B":
        position = "A"
        return position
    elif position == "C":
        position = "B"
        return position
    elif position == "D":
        return position
    
def right(position):
    if position == "1":
        return position
    elif position == "2":
        position = "3"
        return position
    elif position == "3":
        position = "4"
        return position
    elif position == "4":
        return position
    elif position == "5":
        position = "6"
        return position
    elif position == "6":
        position = "7"
        return position
    elif position == "7":
        position = "8"
        return position
    elif position == "8":
        position = "9"
        return position
    elif position == "9":
        return position
    elif position == "A":
        position = "B"
        return position
    elif position == "B":
        position = "C"
        return position
    elif position == "C":
        return position
    elif position == "D":
        return position


current_position = "5"
    

# with open("test.txt") as f:
# # with open("puzzle_input.txt") as f:
#     input = f.read()

l1 = "ULL"
l2 = "RRDDD"
l3 = "LURDL"
l4 = "UUUUD"

l1 = "UULDRRRDDLRLURUUURUURDRUURRDRRURUDRURRDLLDRRRDLRUDULLRDURLULRUUURLDDRURUDRULRDDDUDRDLDDRDDRUURURRDDRLRLUDLUURURLULLLRRDRLDRLRDLULULRDRDDUURUDRRURDLRRDDDLUULDURDLDLLRLRLLUDUDLRDDLUURUUDDRDULDDLDLLDULULRLDDDUDDDRLLRURLRDUUUDUUDDURRDLDDLRDLLUDDLDRLDULDRURLUUDLURLUDRULRLRUUUURLUUUDDULLRLLURDRURLLRLRLDDRURURULRULLUUUULUDULDDDRDDLURLUURRLDDRDRUDDRRLURRDURRLDUULRRLLRDLLDDUURULLRUURRRRDRRURLULLRLRDDULULRDLDDLULLD"
l2 = "UUDUDDRRURRUDDRLDLURURLRLLDRLULLUURLLURDRLLURLLRRLURDLDURUDRURURDLRDRRDULRLLLRDLULDRLLDLDRLDDRUUUUULRLDUURDUUUURUUDLRDLLDRLURULDURURLDLLRDLULLULLLLLUDUDDLRLLLUDLRUUDDUUDUDDDLULDDUDUULUUDUDRRULRRRURUDUUULDDRURLLULLULURLUDRDLUUUDLDRRLRRRULLRRURRUDDDRDLDDDLDUDLLDRRDURRURRURRLDLURUULRLDLUDUDUUULULUUDDDLDDULRDULLULDRDDURRURRRULRDURULUDURRDLLUURRUURLLLULDRRULUUUURLRLRDDDDULLUUUDRRLRRLRRLLLUDDDLRDDURURRDULLLUDLUDURRLRDURUURURDRDUUURURRUDRURRULLDDURRLRRRUULDRLDRRURUDLULRLLRRDLDDRLRRULDDLLUURUDDUDRLUD"
l3 = "DDDUDDRRDRRRUULDRULDLDLURRRUURULRUDDRLLLLURRLRULDLURRULDRUDRRLLLLDULRDLUUURDDLDLURRLLUUURLLUDLUDRRDDULLULURDULRRDLRLDRRUUUUDLRRDLDDLDULDRUULRLLDLRURRUDLDDDRUUULLDDLULDULDUURUDDDLULUDLUURLRURUURDDUDRRLDRRRDDDDRDLUDRRDURDLDRURDDDRRLLLRDDRRRDDLDRLLUURRLDRDDRDLRDDLLDRLRDRDDDURLULLRUURDLULRURRUUDLDRLDRRDDRLDDUULLRDDRRLLLDDDUURDUDRUDUDULDULRUURLDURRDLUURRDLLDDLLURUUUDRLUURRDLUDUULRURLUDDLLRUDURRDRRRDRDLULRRLRUDULUUDRLURRRRLULURRDLLDRDDRLULURDURRDUUULLRDUUDLDUDURUDRUDDLRLULRLRLRRRLRUULLDDLUDDLDRRRLDDLLRLRLRUDULRLLLUULLDRDLDRRDULLRRLLDLDUDULUDDUUDLRDRLUUULLRLDLDDLLRUDDRDD"
l4 = "DDUURRLULDLULULLDUDDRURDDRLRDULUURURRLURDLRRDUUDLULDRDLDLRLULLRULLDRLDRRULUDRLDURUURLLDLLDDLUULLRLRULRLUURDDDDDRLDRLLLDLULDLDLULRRURLLLLLLRLUDLRRLRULUULLLLURDLLRLLDDUDLLULDLLURUUDLRDRDUDDDRDDUULRLLDDDLLRLURLUDLULRRUUUULLDLDLLLDRLUDRDRDLUDLRUDRDRUDRDLLDDLRRLRDLDURDLDRUUUDRLULUULDURDLUUUDDDDDLDRDURDLULDDLLUDUURRUDDLURUDDLRLUUDURUDUULULUDLDLUURDULURURULDDDLUUUUDLUUDUDLLLRDDLRDDLRURRRLLLULLURULLRDLLDRULRDDULULRLUDRRRDULRLLUDUULLRDRDDDULULRURULDLDLDRDLDUDRDULLUUUUUDLRDURDUUULLLRUULLRUULDRRUUDLLLULLUURLDDLUULLRLRLRDRLLLRLURDDURUDUULULDLRLRLLUDURRURDRUDLRDLLRDDRDUULRDRLLRULLUDDRLDLDDDDUDRDD"
l5= "URDLUDUDLULURUDRLUDLUDLRLRLLDDDDDLURURUURLRDUDLRRUUDUURDURUULDRRRDDDLDUURRRDLRULRRDLRUDUDLDDDLLLRLRLRUUUUUULURRRLRLUDULURLDLLDUUDDRUDLDUDRRLULLULLDURDDRRLLRLDLLLLRLULLDDDDLDULLRDUURDUDURRUULLDRULUDLUULUUDDLDDRDLULLULDLDRLDLRULLRLURDURUDRLDURDRULRLLLLURRURLRURUDUDRRUDUUDURDDRRDRLURLURRLDRRLLRLRUDLRLLRLDLDDRDLURLLDURUDDUUDRRLRUDLUDULDRUDDRDRDRURDLRLLRULDDURLUUUUDLUDRRURDDUUURRLRRDDLULLLDLRULRRRLDRRURRURRUUDDDLDRRURLRRRRDLDLDUDURRDDLLLUULDDLRLURLRRURDRUULDDDUDRDRUDRRLRLLLLLURDULDUDRLULDRLUULUDDDDUDDRDDLDDRLLRULRRURDDDRDDLDLULRDDRRURRUDRDDDDRURDRRURUUDUDDUURULLDRDULURUDUD"


line_list = [l1, l2, l3, l4, l5]

for line in line_list:
    for letter in line:
        if letter == "U":
            current_position = up(current_position)
        elif letter == "D":
            current_position = down(current_position)
        elif letter == "L":
            current_position = left(current_position)
        elif letter == "R":
            current_position = right(current_position)

print(current_position)