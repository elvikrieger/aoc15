from collections import Counter

list_all = []
with open("puzzle_input.txt") as f:
# with open("test.txt") as f:
    for line in f:
        line_list = []
        line.replace("\n", "")
        for character in line:
            line_list.append(character)
        # print(line_list)
        line_list.pop()
        list_all.append(line_list)

# print(list_all)
sloupce = list(zip(*list_all))

# most_common = sloupce.most_common()
# print(sloupce)

most_common_letters = []
for jeden_sloupec in sloupce:
    sloupec = Counter(jeden_sloupec)
    most_common_letters.append((sloupec.most_common()[-1]))

result_letters = list(zip(*most_common_letters))[0]
print(result_letters)