from collections import Counter

letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']


def sort_by_count_then_alphabet(list):
    sorted_list = sorted(list, key=lambda x: (-x[1], x[0]))
    sorted_list = sorted_list[:5]
    print(sorted_list)
    return sorted_list

def split_name(room):
    name, checksum = room.split("[", 1)
    return {"name": name, "checksum": checksum}

def cypher(text, number):
    shifted_text = ""
    for one_character in text:                    
            if one_character in letters:
                index = letters.index(one_character)
                
                shifted_index = index + (number%26)
                shifted_text += letters[shifted_index]
    return shifted_text

def is_real_room(room_input):
    """porovná nejčastější písmena v názvu seřazené výskytu, případně podle abecedy, s písmeny v [], pokud je real, vrátí hodnotu ID"""

    # room_code je název místnosti bez room id(čísel na konci)
    room_code = split_name(room_input)["name"]
    room_code = room_code.replace("-", "")
    room_code = room_code.replace("\n", "")
    room_code = room_code[:-3]

    room_checksum = split_name(room_input)["checksum"][:-1]
    possible_id = split_name(room_input)["name"][-3:]
    print(room_code, room_checksum, possible_id)


    room_cyphered_code = cypher(room_code, int(possible_id))
    room = Counter(room_cyphered_code)
    room_most_common = room.most_common()
    # room most common je list tuplů s písmeny seřazenými podle výskytu 


    most_common_letters = list(zip(*sort_by_count_then_alphabet(room_most_common)))[0]
    most_common_letters = "".join(most_common_letters)

    if room_checksum == most_common_letters[:5]:
        room_id = possible_id
    else:
        room_id = 0
    return room_id


# print(cypher("qzmt-zixmtkozy-ivhz", 343))
print(is_real_room("qzmt-zixmtkozy-ivhz-343[zimth]"))

# room = Counter("qzmt-zixmtkozy-ivhz-343[zimth]")

# print(room.most_common())