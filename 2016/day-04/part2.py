from collections import Counter

letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
letters = letters*1000
def sort_by_count_then_alphabet(list):
    sorted_list = sorted(list, key=lambda x: (-x[1], x[0]))
    sorted_list = sorted_list[:5]
    # print(sorted_list)
    return sorted_list

def split_name(room):
    name, checksum = room.split("[", 1)
    return {"name": name, "checksum": checksum}

def cypher(text, number):
    shifted_text = ""
    for one_character in text:                    
            if one_character in letters:
                index = letters.index(one_character)
                
                shifted_index = index + (number%26)
                shifted_text += letters[shifted_index]
            else:
                 shifted_text +=  one_character
                
    return shifted_text

def is_real_room(room_input):
    """porovná nejčastější písmena v názvu seřazené výskytu, případně podle abecedy, s písmeny v [], pokud je real, vrátí hodnotu ID"""

    # room_code je název místnosti bez room id(čísel na konci)
    room_code = split_name(room_input)["name"]
    # room_code = room_code.replace("-", "")
    room_code = room_code.replace("\n", "")
    room_code = room_code[:-3]

    room_checksum = split_name(room_input)["checksum"][:-1]
    room_checksum = split_name(room_input)["checksum"][:-1]
    room_checksum = room_checksum.replace("-", "")
    room_checksum = room_checksum.replace("\n", "")
    room_checksum = room_checksum.replace("]", "")
    room_checksum = room_checksum.replace("[", "")

    possible_id = split_name(room_input)["name"][-3:]
    # print(room_code, room_checksum, possible_id)


    room_cyphered_code = cypher(room_code, int(possible_id))
    if "northpoleobjects" in room_cyphered_code:
        print(possible_id)
    else:
        pass


with open("puzzle_input.txt") as f:
# with open("test.txt") as f:
    real_room_ids = 0
    for line in f:
        # print(line)
        is_real_room(line)
        # print(is_real_room(line))

