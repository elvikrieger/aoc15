from collections import Counter


def sort_by_count_then_alphabet(list):
    sorted_list = sorted(list, key=lambda x: (-x[1], x[0]))
    sorted_list = sorted_list[:5]
    # print(sorted_list)
    return sorted_list

def split_name(room):
    name, checksum = room.split("[", 1)
    return {"name": name, "checksum": checksum}

def is_real_room(room_input):
    """porovná nejčastější písmena v názvu seřazené výskytu, případně podle abecedy, s písmeny v [], pokud je real, vrátí hodnotu ID"""
    room_input = room_input.replace("-", "")
    room_input = room_input.replace("\n", "")
    room_input = room_input.replace(" ", "")

    room_code = split_name(room_input)["name"]
    room_code = room_code.replace("-", "")
    room_code = room_code.replace("\n", "")
    room_code = room_code[:-3]

    room_checksum = split_name(room_input)["checksum"][:-1]
    room_checksum = room_checksum.replace("-", "")
    room_checksum = room_checksum.replace("\n", "")

    possible_id = split_name(room_input)["name"][-3:]
    print(room_code, room_checksum, possible_id)
    room = Counter(room_code)
    room_most_common = room.most_common()

    most_common_letters = list(zip(*sort_by_count_then_alphabet(room_most_common)))[0]
    most_common_letters = "".join(most_common_letters)

    if room_checksum == most_common_letters[:5]:
        room_id = int(possible_id)
    else:
        room_id = 0
    return room_id

with open("puzzle_input.txt") as f:
# with open("test.txt") as f:
    real_room_ids = 0
    for line in f:
        # print(line)
        real_room_ids += is_real_room(line)
        # print(is_real_room(line))

print(real_room_ids)


