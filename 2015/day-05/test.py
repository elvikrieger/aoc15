ok_vowels = ["a", "e", "i", "o", "u"]


def contains_three_vowels(string):
    string_contains_3_vowels = 0
    
    for letter in ok_vowels:
        if letter in string:
            vowel_count = string.count(letter)
            string_contains_3_vowels += vowel_count
    if string_contains_3_vowels > 2:
        return True
    else:
        return False
    
print(contains_three_vowels("oooo"))