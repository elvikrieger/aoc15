# A nice string is one with all of the following properties:

# It contains at least three vowels (aeiou only), like aei, xazegov, or aeiouaeiouaeiou.
# It contains at least one letter that appears twice in a row, like xx, abcdde (dd), or aabbccdd (aa, bb, cc, or dd).
# It does not contain the strings ab, cd, pq, or xy, even if they are part of one of the other requirements.

ok_vowels = ["a", "e", "i", "o", "u"]
doubleletters = ["aa", "bb", "cc", "dd", "ee", "ff", "gg", "hh", "ii", "jj", "kk", "ll", "mm", "nn", "oo", "pp", "qq", "rr", "ss", "tt", "uu", "vv", "ww", "xx","yy" , "zz"]
not_ok_list = ["ab", "cd", "pq", "xy"]

def contains_three_vowels(string):
    string_contains_3_vowels = 0
    
    for letter in ok_vowels:
        if letter in string:
            vowel_count = string.count(letter)
            string_contains_3_vowels += vowel_count
    if string_contains_3_vowels > 2:
        return True
    else:
        return False


def contains_double_letters(string):
    string_contains_double = 0
    for doublestr in doubleletters:
        if doublestr in string:
            string_contains_double +=1
    if string_contains_double > 0:
        return True
    else:
        return False

def doesnt_contain_not_ok(string):
    string_contains_not_ok = 0
    for not_ok in not_ok_list:
        if not_ok in string:
            string_contains_not_ok +=1
    if string_contains_not_ok > 0:
        return False
    else:
        return True

def naughty_or_nice(string):
    if contains_three_vowels(string) == True and contains_double_letters(string) == True and doesnt_contain_not_ok(string) == True:
        return "Nice"
    

nice_list = []
naughty_list = []
with open("puzzle_input.txt") as f:
# with open("test.txt") as f:
    nice = 0
    for line in f:
        if naughty_or_nice(line) == "Nice":
            nice +=1
        else:
            naughty_list.append(line) 

print(nice)
