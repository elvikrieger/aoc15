with open("puzzle_input.txt") as f:
    input = f.read()
    # input = "^>v<"

    souradnice = [0,0]
    souradnice_2 = [0,0]
    souradnice_list = ["0,0"]
    i = 0
    for x in input:
        i +=1
        if i%2 == 1:               
            if x == ">":
                souradnice_2[0]+=1
            if x == "<": 
                souradnice_2[0]-=1
            if x == "^":
                souradnice_2[1]+=1
            if x == "v":
                souradnice_2[1]-=1
            souradnice_list.append(str(souradnice_2[0])+","+str(souradnice_2[1]))
        else:
            if x == ">":
                souradnice[0]+=1
            if x == "<": 
                souradnice[0]-=1
            if x == "^":
                souradnice[1]+=1
            if x == "v":
                souradnice[1]-=1
            souradnice_list.append(str(souradnice[0])+","+str(souradnice[1]))
        # print(souradnice)

print(len(set(souradnice_list)))