def split_lines(line):
    line = line.strip().split()

    if line[0] == "turn":
        line.remove("turn")
    mode = line[0]

    first_light = line[1].split(",")
    for coord in first_light:
        first_light[first_light.index(coord)] = int(coord)

    second_light = line[3].split(",")
    for coord in second_light:
        second_light[second_light.index(coord)] = int(coord)
    return mode, first_light, second_light


def turn_on(coord1, coord2):
    for x in range(coord1[0], coord2[0] + 1):
        for y in range(coord1[1], coord2[1] + 1):
            lights_dict[(x, y)] += 1


def turn_off(coord1, coord2):
    for x in range(coord1[0], coord2[0] + 1):
        for y in range(coord1[1], coord2[1] + 1):
            if lights_dict[(x, y)] > 0:
                lights_dict[(x, y)] -= 1
            else:
                pass


def toggle(coord1, coord2):
    for x in range(coord1[0], coord2[0] + 1):
        for y in range(coord1[1], coord2[1] + 1):
            lights_dict[(x, y)] += 2


lights_dict = {}

for x in range(0, 1000):
    for y in range(0, 1000):
        lights_dict[(x, y)] = 0

with open("puzzle_input.txt") as f:
# with open("test.txt") as f:
    for line in f:
        mode, first_light, second_light = split_lines(line)
        if mode == "on":
            turn_on(first_light, second_light)
        elif mode == "off":
            turn_off(first_light, second_light)
        else:
            toggle(first_light, second_light)


lit = 0
for light in lights_dict:
    lit += lights_dict[light]

print(lit)
