# Part 2:
# A present with dimensions 2x3x4 requires 2+2+3+3 = 10 feet of ribbon to wrap the present plus 2*3*4 = 24 feet of ribbon for the bow, 
#           for a total of 34 feet.
# A present with dimensions 1x1x10 requires 1+1+1+1 = 4 feet of ribbon to wrap the present plus 1*1*10 = 10 feet of ribbon for the bow, 
#           for a total of 14 feet.

def surface_area(lenght, width, height):
    side1 = lenght*width
    side2 = width*height
    side3 = height*lenght
    area = 2*side1 + 2*side2 + 2*side3 + min(side1, side2, side3)
    return area

def split_numbers(line):
    lenght, width, height = line.split("x")
    return [int(lenght), int(width), int(height)]

def ribbon_for_box(lenght, width, height):
    box_size = [lenght, width, height]
    box_sides = sorted(box_size)
    wrap = 2*box_sides[0] + 2*box_sides[1]
    bow = lenght*width*height
    ribbon = wrap+bow
    return ribbon

# with open("test.txt") as f:
with open("puzzle_input.txt") as f:
    box_list = []
    wrapping_paper = 0
    ribbon = 0
    for line in f:
        box_list.append(split_numbers(line[:-1]))
    for box in box_list:
        wrapping_paper += surface_area(box[0],box[1], box[2])
        ribbon += ribbon_for_box(box[0],box[1], box[2])
print(ribbon)