# Advent of Code - Elvi Krieger


Advent of Code is an annual advent calendar of programming challenges. The goal is to solve various problems that progressively increase in difficulty.

This project contains solutions for all Advent of Code challenges that I have solved.


### Advent of Code 2024
*First year I actively participated, and currently I have 37 stars.*
- [01 ★★ ](https://gitlab.com/elvikrieger/aoc/-/blob/main/2024/01/01.py?ref_type=heads)
- [02 ★★](https://gitlab.com/elvikrieger/aoc/-/blob/main/2024/02/02.py?ref_type=heads)
- [03 ★★](https://gitlab.com/elvikrieger/aoc/-/blob/main/2024/03/03.py?ref_type=heads)
- [04 ★★](https://gitlab.com/elvikrieger/aoc/-/blob/main/2024/04/04.py?ref_type=heads)
- [05 ★★](https://gitlab.com/elvikrieger/aoc/-/blob/main/2024/05/05.py?ref_type=heads)
- [06 ★★](https://gitlab.com/elvikrieger/aoc/-/tree/main/2024/06?ref_type=heads)
- [07 ★★](https://gitlab.com/elvikrieger/aoc/-/blob/main/2024/07/07.py?ref_type=heads)
- [08 ★★](https://gitlab.com/elvikrieger/aoc/-/tree/main/2024/08?ref_type=heads)
- [09 ★★](https://gitlab.com/elvikrieger/aoc/-/tree/main/2024/09?ref_type=heads)
- [10 ★★](https://gitlab.com/elvikrieger/aoc/-/blob/main/2024/10/10.py?ref_type=heads)
- [11 ★★](https://gitlab.com/elvikrieger/aoc/-/blob/main/2024/11/p1.py?ref_type=heads)
- [12 ★](https://gitlab.com/elvikrieger/aoc/-/tree/main/2024/12?ref_type=heads)
- [13 ★★](https://gitlab.com/elvikrieger/aoc/-/blob/main/2024/13/part1.py?ref_type=heads)
- [14 ★★](https://gitlab.com/elvikrieger/aoc/-/tree/main/2024/14?ref_type=heads)
- [15 ★](https://gitlab.com/elvikrieger/aoc/-/tree/main/2024/15?ref_type=heads)
- 16
- [17 ★](https://gitlab.com/elvikrieger/aoc/-/blob/main/2024/17/p1.py?ref_type=heads)
- [18 ★★](https://gitlab.com/elvikrieger/aoc/-/blob/main/2024/18/p1.py?ref_type=heads)
- [19 ★★](https://gitlab.com/elvikrieger/aoc/-/tree/main/2024/19?ref_type=heads)
- 20
- 21
- [22 ★](https://gitlab.com/elvikrieger/aoc/-/tree/main/2024/22?ref_type=heads)
- [23 ★](https://gitlab.com/elvikrieger/aoc/-/tree/main/2024/23?ref_type=heads)
- [24 ★](https://gitlab.com/elvikrieger/aoc/-/tree/main/2024/24?ref_type=heads)
- [25 ★](https://gitlab.com/elvikrieger/aoc/-/blob/main/2024/25/p1.py?ref_type=heads)


### Previous years
- [Advent of Code 2023](https://gitlab.com/elvikrieger/aoc/-/tree/main/2023?ref_type=heads) ★★★★★
- [Advent of Code 2018](https://gitlab.com/elvikrieger/aoc/-/tree/main/2018?ref_type=heads) ★★★★★
- [Advent of Code 2017](https://gitlab.com/elvikrieger/aoc/-/tree/main/2017?ref_type=heads) ★★★★★★★★★★★★★★★★
- [Advent od Code 2016](https://gitlab.com/elvikrieger/aoc/-/tree/main/2016?ref_type=heads) ★★★★★★★★
- [Advent of Code 2015](https://gitlab.com/elvikrieger/aoc/-/tree/main/2015?ref_type=heads) ★★★★★★★★★★★★